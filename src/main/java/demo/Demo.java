package demo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.JSONObject;

public class Demo 
{
	final static Logger logger=Logger.getLogger(Demo.class);
	public static void main(String[] args) 
	{
		
		
		HttpClient client;
		HttpGet request;


		HttpResponse response;

		// Get the response
		BufferedReader rd;
		try {
			client = new DefaultHttpClient();
			request = new HttpGet("https://api.iextrading.com/1.0/stock/market/batch?symbols=aapl&types=quote");
			 response = client.execute(request);
			
			rd = new BufferedReader
			    (new InputStreamReader(
			    response.getEntity().getContent()));
			
			String line = "";
			StringBuffer responsedata = new StringBuffer();
			while ((line = rd.readLine()) != null) {
			    responsedata.append(line);
			    }
			
			JSONObject jsonObj=new JSONObject(responsedata.toString());
			JSONObject name1=jsonObj.getJSONObject("AAPL");
			JSONObject name2=name1.getJSONObject("quote");
			
			
		logger.info("this is info:" + "symbol");
		
		System.out.println(name2.get("companyName"));
		
		logger.debug("this is debug" + "primary exchange");
		
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

